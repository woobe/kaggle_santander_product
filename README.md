# Kaggle Santander Product Recommendation

## About the Competition

- [Kaggle page](https://www.kaggle.com/c/santander-product-recommendation)
- Our Team: [Overfitting Consortium](https://www.kaggle.com/t/410668/overfitting-consortium)
- Final Position: 38th / 1785

## About this Repository

You can find a simplified version of [Joe](http://www.jofaichow.co.uk/)'s data processing, H2O GBM models and ensembles here.

The files presented here are mainly for future benchmarking purposes.

Our final Kaggle solution also includes additional features and XGBOOST models from ZFTurbo 
(which are not hosted here).

## Quick Results Summary (I will expand this later ...)

Average logloss (5-Fold CV)

- Stage One (Level One): 0.9972088
- Stage Two (Stacking) : 0.9862233

Average logloss (Holdout)

- Stage One (Level One): 0.9499901
- Stage Two (Stacking) : 0.9376986


## Software Used

- R version 3.2.3 (2015-12-10)
- data.table_1.10.0 
- h2o_3.10.2.1    
- dplyr_0.5.0 
- readr_1.0.0
- lubridate_1.6.0

## Platform

- aarch64-unknown-linux-gnu (64-bit) (Run 1) Running under: Ubuntu 16.04.1 LTS
- x86_64-pc-linux-gnu (64-bit) (Run 2) Running under: Ubuntu 16.04.1 LTS

## Data Processing

- Raw Data 
    - not inlcuded, please download from Kaggle and put them into `data` if you want to run `data_prep.R`.
    - `data/train_ver2.csv`
    - `data/test_ver2.csv`

- R Script
    - This script reformats raw data and exports three files for modelling.
    - `code/data_prep.R`

- Reformatted data
      - `data/d_train.csv.gz`
      - `data/d_valid.csv.gz`
      - `data/d_test.csv.gz`


## Stage One:

- Building five slightly different H2O GBM models with random grid search and 5-fold CV.
- Using the CV holdout predictions as level one data for stacking in stage two.
- Main script: `code/h2o_gbm_L1.R`

As I recently discovered a new cloud service called [Packet](https://www.packet.net/bare-metal/) 
with some free credits, I was keen to run my code on two different servers: 

- **Type 2A**: 96 Physical Cores @ 2.0 GHz (2 × Cavium ThunderX) + 128GB RAM, $0.50 / hr
- **Type 2**: 24 Physical Cores @ 2.2 GHz  (2 × E5-2650 v4) + 256GB RAM, $1.25 / hr

- `output/h2o_gbm_L1_run1`: results from a run on Type 2A
- `output/h2o_gbm_L1_run2`: results from a run on Type 2

This is interesting as I would like to find out ... 

- whether H2O can run on ARMv8 and return the correct results
- if there is a sweet spot of speed/cost trade-off 

I may continue to run this on different cloud services (AWS/Google/Azure) in the future.


## Stage Two:

- Using the level one data for model stacking
- Stacking does not improve out-of-bag performance significantly because it is only
based on similar H2O GBM models.
- (But every little helps - this is Kaggle!)
- Main script: `code/h2o_gbm_L2.R`




